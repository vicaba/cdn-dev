package main


import akka.actor.{Props, ActorSystem}
import actors._
import transaction.ReadWriteTransaction
import transaction.operation.{CreateAttributeOperation, WriteOperation}
import net.message.{ContentMessage}


/**
 * Created by Work on 11/11/14.
 */
object Main {

  def main (args: Array[String]) {
    val system = ActorSystem("firstSystem")
/*
    val cdnMaster = system.actorOf(Props(new CDNMaster), "cdnMaster")

    val myFirstNode = system.actorOf(Props(new CoreNode), "node1")
    val myFirstNodeToSecond = system.actorOf(Props(new PortNode with Eager with Active), "node1-2")
    val myFirstNodeToThird = system.actorOf(Props(new PortNode with Eager with Passive), "node1-3")

    val mySecondNode = system.actorOf(Props(new CoreNode), "node2")
    val mySecondNodeToFirst = system.actorOf(Props(new PortNode with Eager with Active), "node2-1")
    val mySecondNodeToThird = system.actorOf(Props(new PortNode with Eager with Passive), "node2-3")

    val myThirdNode = system.actorOf(Props(new CoreNode), "node3")
    val myThirdNodeToFirst = system.actorOf(Props(new PortNode with Eager with Passive), "node3-1")
    val myThirdNodeToSecond = system.actorOf(Props(new PortNode with Eager with Active), "node3-2")

    // First Node
    myFirstNode ! NewNode(myFirstNodeToSecond)
    myFirstNodeToSecond ! NewNode(myFirstNode)
    myFirstNode ! NewNode(myFirstNodeToThird)
    myFirstNodeToThird ! NewNode(myFirstNode)

    // Second Node
    mySecondNode ! NewNode(mySecondNodeToFirst)
    mySecondNodeToFirst ! NewNode(mySecondNode)
    mySecondNode ! NewNode(mySecondNodeToThird)
    mySecondNodeToThird ! NewNode(mySecondNode)

    // Third Node
    myThirdNode ! NewNode(myThirdNodeToFirst)
    myThirdNodeToFirst ! NewNode(myThirdNode)
    myThirdNode ! NewNode(myThirdNodeToSecond)
    myThirdNodeToSecond ! NewNode(myThirdNode)

    // First-Second
    myFirstNodeToSecond ! NewNode(mySecondNodeToFirst)
    mySecondNodeToFirst ! NewNode(myFirstNodeToSecond)

    // First-Third
    myFirstNodeToThird ! NewNode(myThirdNodeToFirst)
    myThirdNodeToFirst ! NewNode(myFirstNodeToThird)

    // Second-Third
    mySecondNodeToThird ! NewNode(myThirdNodeToSecond)
    myThirdNodeToSecond ! NewNode(mySecondNodeToThird)

*/

    val cdnMaster = system.actorOf(Props(new CDNMaster), "cdnMaster")

    val myFirstNode = system.actorOf(Props(new CoreNode), "node1")
    val mySecondNode = system.actorOf(Props(new CoreNode), "node2")
    val myThirdNode = system.actorOf(Props(new CoreNode), "node3")

    cdnMaster ! AddNode(myFirstNode, 0, AddNode.WriteMode)
    cdnMaster ! AddNode(mySecondNode, 0, AddNode.WriteMode)
    cdnMaster ! AddNode(myThirdNode, 0, AddNode.WriteMode)

    cdnMaster ! RequestNodeConnection((myFirstNode,HowAlgorithm.Active,WhenAlgorithm.Eager),(mySecondNode,HowAlgorithm.Active,WhenAlgorithm.Eager))
    cdnMaster ! RequestNodeConnection((myFirstNode,HowAlgorithm.Active,WhenAlgorithm.Eager),(myThirdNode,HowAlgorithm.Active,WhenAlgorithm.Eager))
    cdnMaster ! RequestNodeConnection((mySecondNode,HowAlgorithm.Active,WhenAlgorithm.Eager),(myThirdNode,HowAlgorithm.Active,WhenAlgorithm.Eager))

    cdnMaster ! PartitionUpdate(0)

    cdnMaster ! ContentMessage(0, ReadWriteTransaction(0, List(CreateAttributeOperation("a"), WriteOperation("a", "=", 3)),0))

    //system.terminate()

  }

}
