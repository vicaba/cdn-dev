package net.message

/**
 * Created by Work on 12/11/14.
 */

trait Message

trait MessageContent

sealed case class ContentMessage(vectorClock: Double, content: MessageContent) extends Message

trait ControlMessage extends Message

trait ACK extends ControlMessage

trait DiscoveryAndLookupMessage extends ControlMessage

trait ManageConnection extends ControlMessage

class NewConnection(val id: Double) extends ManageConnection