package transaction.operation

import scala.collection.mutable
import scala.collection.immutable.List
import org.json.JSONObject
import org.json.JSONArray

/**
 * An Operation
 * @param attribute the attribute against the operation will be executed
 */
class Operation(val attribute: String)

sealed case class CreateAttributeOperation(override val attribute: String) extends Operation(attribute)

/**
 *  A read operation
 * @param from  the variable to read from
 */
sealed case class ReadOperation(from: String) extends Operation(from)

/**
 * A write oeration
 * @param to  the variable to transform
 * @param operation the operation
 * @param value the value of the operand
 */
sealed case class WriteOperation(to: String, operation: String, value: Int) extends Operation(to)

object Operations {

  /**
   * Creates an Operation from a json object
   * @param jsonString  the json string representing an operation
   * @return  the operation
   */
  def fromJSONObject(jsonString: String): Operation = {
    val jsonObject = new JSONObject(jsonString)

    val operationType = jsonObject getInt "type"
    operationType match {
      case 0 => ReadOperation(jsonObject getString "from")
      case 1 => WriteOperation(jsonObject getString "to", jsonObject getString "op", jsonObject getInt "value")
    }

  }

  /**
   * Creates a list of Operations from a json array
   * @param jsonString  the json array of operation objects
   * @return  the list of operations
   */
  def fromJSONArray(jsonString: String): List[Operation] = {
    val operations = new mutable.MutableList[Operation]

    val jsonArray = new JSONArray(jsonString)

    for(i <- 0 until jsonArray.length()) {
      operations += Operations fromJSONObject(jsonArray get i toString())
    }

    operations.toList

  }

  def main(args: Array[String]) {

    val operations = Operations fromJSONArray "[{\"type\": 1, \"to\": \"a\", \"op\": \"eq\", \"value\": \"2\"}]"

    operations foreach {
      case ReadOperation(from) => println("ReadOperation -> read from " + from)
      case WriteOperation(to, operation, value) => println("WriteOperation -> write " + to + " " + operation + " " + value)
    }

  }

}