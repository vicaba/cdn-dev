package transaction

import transaction.operation._

import scala.collection.{immutable, mutable}
import scala.collection.mutable.ListBuffer

case class NoOperation() extends Operation("noAttr")

object DB {

  type Attribute = String

  type Value = Int

  def main(args: Array[String]) {
    val db = new DB

    db.executeOperation(CreateAttributeOperation("a"))
    var row = db.executeOperation(ReadOperation("a"))
    println(row.value)
    row = db.executeOperation(WriteOperation("a", "+", 2))
    println(row.value)
    row = db.executeOperation(WriteOperation("a", "-", 3))
    println(row.value)

  }
}

class DB {

  case class Row(attribute: DB.Attribute, value: DB.Value)

  case class LogRow(operation: Operation, rowSnapshot: Row)

  val attributes = new mutable.HashMap[DB.Attribute, ListBuffer[LogRow]]()

  val logHistory = new ListBuffer[LogRow]()

  def executeOperation(operation: Operation): Row = {

    def logOperation(operation: Operation, row: Row): LogRow = {
      val logRow = LogRow(operation, row)
      attributes.get(operation.attribute) match {
        case None =>
          throw new IllegalAccessError
        case Some(log) =>
          logHistory.+=(logRow)
          log.+=(logRow)
          logRow
      }
    }

    def createAttribute(operation: Operation): Row = {
      val log = new ListBuffer[LogRow]()
      val row = Row(operation.attribute, 0)
      attributes += operation.attribute -> log
      logOperation(operation, row)
      row
    }

    def updateAttribute(): Row = {

      def modifyAttribute(row: Row, op: String, opValue: DB.Value): Row = {
        op match {
          case "=" =>
            Row(row.attribute, opValue)
          case "-" =>
            Row(row.attribute, row.value - opValue)
          case "+" =>
            Row(row.attribute, row.value + opValue)
        }
      }

      val opAttr = operation.attribute

      attributes.get(opAttr) match {
        case None => throw new IllegalAccessError
        case Some(attrLog) =>
          val row = attrLog.last.rowSnapshot
          operation match {
            case op: ReadOperation =>
              row
            case op: WriteOperation =>
              // Modify the attribute
              val newRow = modifyAttribute(row, op.operation, op.value)

              // Log the changes
              logOperation(operation, newRow)

              newRow
            case _ => row //TODO: Arreglar
          }
      }
    }

    operation match {
      case op: CreateAttributeOperation => createAttribute(op)
      case op: Operation => updateAttribute()

    }

  }

  /**
   * Executes the transaction against the DB
   * @param transaction the transaction to execute
   * @return  a compressed transaction
   */
  def executeTransaction(transaction: Transaction): Transaction = {
    var modifiedAttributes: List[DB.Attribute] = Nil
    var createdAttributes: List[CreateAttributeOperation] = Nil
    transaction.operations.foreach {
      operation =>
        executeOperation(operation)
        operation match {
          case WriteOperation(_, _, _) => modifiedAttributes = modifiedAttributes.+:(operation.attribute)
          case CreateAttributeOperation(attr) => createdAttributes = createdAttributes.+:(CreateAttributeOperation(attr))
          case _ =>
        }
    }

    modifiedAttributes = modifiedAttributes.distinct

    val operations = createdAttributes ++ modifiedAttributes.map {
      attr =>
        getAttribute(attr) match {
          case Some(row) => WriteOperation(row.attribute, "=", row.value)
          case None => NoOperation()
        }
    }.distinct.toList

    new Transaction(transaction.id, operations, transaction.idPart)

  }

  def getGlobalLog: Option[List[LogRow]] = logHistory.isEmpty match {
    case true => None
    case false => Some(logHistory.toList)
  }

  def getAttributeLog(attribute: DB.Attribute): Option[List[LogRow]] = attributes.get(attribute) match {
    case None => None
    case Some(list) => Some(list.toList)
  }

  def getAttribute(attribute: DB.Attribute): Option[Row] = {
    getAttributeLog(attribute) match {
      case None => None
      case Some(list) => Some(list.last.rowSnapshot)
    }
  }
}
