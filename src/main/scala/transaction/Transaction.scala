package transaction

import java.io.{FileReader, BufferedReader}

import org.json.JSONObject

import scala.collection.immutable.List
import transaction.operation.{ReadOperation, Operations, Operation}
import net.message.MessageContent

/**
 * A transaction with a list of Operations
 * @param id  the transaction id
 * @param operations  the list of Operations
 * @param idPart  the id of the partition the transaction is aimed to write or read
 */
sealed class Transaction(val id: Int, val operations: List[Operation], val idPart: Int) extends MessageContent

/**
 * A transaction containing only read operations
 * @param id  the transaction id
 * @param operations  the list of operations
 * @param idPart  the id of the partition the transaction is aimed to write or read
 */
sealed case class ReadOnlyTransaction(override val id: Int, override val operations: List[Operation], override val idPart: Int) extends Transaction(id, operations, idPart)

/**
 * A transaction containing read and write operations
 * @param id  the transaction id
 * @param operations  the list of operations
 * @param idPart  the id of the partition the transaction is aimed to write or read
 */
sealed case class ReadWriteTransaction(override val id: Int, override val operations: List[Operation], override val idPart: Int) extends Transaction(id, operations, idPart)

object Transactions {

  /**
   * Creates a transaction from a json object
   * @param jsonString  the json string representing a transaction
   * @return  the transaction
   */
  def fromJSONObject(jsonString: String): Transaction = {
    val jsonObject = new JSONObject(jsonString)

    val id = jsonObject.getInt("id")
    val jsonOperations = jsonObject.getJSONArray("operations")
    val idPart = jsonObject.getInt("idPart")

    val operations = Operations.fromJSONArray(jsonOperations.toString)

    operations.forall {
      case ReadOperation(_) => true
      case _ => false
    } match {
      case true => ReadOnlyTransaction(id, operations, idPart)
      case false => ReadWriteTransaction(id, operations, idPart)
    }
  }

  def main(args: Array[String]) {
    val transactionHardCoded = Transactions.fromJSONObject("{ \"id\": 1, \"operations\": [{\"type\": 0, \"from\": \"a\"}, {\"type\": 1, \"to\": \"a\", \"op\": \"eq\", \"value\": 5}], \"idPart\": 1}")
    transactionHardCoded match {
      case ReadOnlyTransaction(_,_,_) => println("ReadOnly transaction")
      case ReadWriteTransaction(_,_,_) => println("ReadWrite transaction")
    }

    var transaction: Transaction = null
    var line: String = null
    try {
      val reader = new BufferedReader(new FileReader("./docs/transactions"))
      line = reader.readLine()
      while (line != null) {
        println("Read: " + line)
        transaction = Transactions.fromJSONObject(line)
        println("Size: " + transaction.operations.size)
        line = reader.readLine()
      }
      reader.close()
    } catch {
      case e: Exception => e.printStackTrace()
    }
  }
}