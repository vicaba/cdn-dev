package actors

import akka.actor.{Props, Actor, ActorRef}
import akka.util.Timeout
import net.message._
import transaction.Transaction
import akka.pattern.ask
import scala.concurrent.duration._

import scala.collection.mutable

// Define the node specific messages
/**
 * sender() has received a transaction
 * @param transactionId the transaction id
 */
case class TransactionReceived(transactionId: Int) extends ACK

/**
 * sender() sends his vectorClock value
 * @param vectorClock the vectorClock value
 */
case class VectorClockUpdate(vectorClock: VectorClock.clockType) extends ControlMessage

/**
 * It says: "Connect a Node (node) to this node"
 * @param node  the node to connect to
 */
case class NewNode(node: ActorRef) extends DiscoveryAndLookupMessage

case class ReplicationType_?() extends DiscoveryAndLookupMessage

case class ReplicationType(replicationType: String) extends DiscoveryAndLookupMessage

/**
 * Message to create a new connection between node1 and node2 with the specified protocols
 * @param node1 Tuple of ActorRef, HowAlgorithm and WhenAlgorithm
 * @param node2 Tuple of ActorRef, HowAlgorithm and WhenAlgorithm
 */
case class RequestNodeConnection(node1: (ActorRef,String,String), node2: (ActorRef,String,String)) extends ManageConnection
case class RequestNewConnection(override val id: Double, factoryMethod: () => PortNode) extends NewConnection(id)
case class ResponseConnectionCreated(override val id: Double, port: ActorRef) extends NewConnection(id)

object WhenAlgorithm {
  val Eager = "Eager"
  val Lazy = "Lazy"
}

/**
 * Abstract trait for the implementation of input replication algorithms, attach it to a Node
 */
trait WhenAlgorithm {
  self: Node =>

  /**
   * The queue of Transactions that this Node needs to receive an acknowledgement
   */
  protected lazy val pendingQueue: mutable.LinkedHashMap[Int, PendingTransaction] = mutable.LinkedHashMap[Int, PendingTransaction]()

  protected lazy val tempTransactions = mutable.LinkedHashMap[Int, TempTransaction]()

  def persistTransaction(transaction: Transaction): (Option[Transaction],Option[Transaction])

  /**
   * Call when the node receives a transaction
   * @param transaction the transaction
   * @param sender  the sender of the transaction
   */
  def doWhenTransactionReceived(transaction: Transaction, sender: ActorRef)

  protected def dealWithNewTransaction(transaction: Transaction) = {
    println(this.self.path + ": Transaction " + transaction.id + " received!")
    addPendingTransaction(transaction, sender())
    val transactions = persistTransaction(transaction)
    propagateTransaction(transactions._1, transactions._2)
  }

  /**
   * Call when an TransactionReceived message is received by a Node
   * @param transactionId the transaction id
   */
  def doWhenTransactionReceivedACK(transactionId: Int)

  protected def addPendingTransaction(transaction: Transaction, respondTo: ActorRef) = {
    pendingQueue += transaction.id -> PendingTransaction(transaction, respondTo, linkedToNodes - sender)
    tempTransactions += transaction.id -> TempTransaction(transaction, vectorClock.value())
  }

  /**
   * Call when the Node has to propagate the transaction to the linked nodes
   */
  def propagateTransaction(fullTransaction: Option[Transaction], reducedTransaction: Option[Transaction]) = {
    if((fullTransaction orElse reducedTransaction).isDefined) {
      vectorClock inc()
      (linkedToNodes - sender()).foreach {
        case (node, howAlgorithm) =>
          howAlgorithm match {
            case HowAlgorithm.Active =>
              node ! ContentMessage(vectorClock.value(), (fullTransaction orElse reducedTransaction).get)
            case HowAlgorithm.Passive =>
              node ! ContentMessage(vectorClock.value(), (reducedTransaction orElse fullTransaction).get)
            case HowAlgorithm.Core =>
              node ! ContentMessage(vectorClock.value(), (reducedTransaction orElse fullTransaction).get)
          }
          println("I'm " + this.self.path + ". Propagated transaction " + (fullTransaction orElse reducedTransaction).get.id + " to " + node.path)
      }
    }
  }

  def isAlreadyReceived(id: Int): Boolean = tempTransactions.contains(id)

}

/**
 * Lazy replication algorithm
 */
trait Lazy extends WhenAlgorithm {
  self: Node =>

  def doWhenTransactionReceived(transaction: Transaction, sender: ActorRef) = {

    isAlreadyReceived(transaction.id) match {
      case true => sender ! TransactionReceived(transaction.id)
      case false => dealWithNewTransaction(transaction)
    }

  }

  override protected def dealWithNewTransaction(transaction: Transaction) = {
    super.dealWithNewTransaction(transaction)
    sender ! TransactionReceived(transaction.id)
  }

  def doWhenTransactionReceivedACK(transactionId: Int) = pendingQueue.remove(transactionId)
}

/**
 * Eager replication algorithm
 */
trait Eager extends WhenAlgorithm {
  self: Node =>

  def doWhenTransactionReceived(transaction: Transaction, sender: ActorRef) = {

    isAlreadyReceived(transaction.id) match {
      case true => sender ! TransactionReceived(transaction.id)
      case false => dealWithNewTransaction(transaction)
    }

  }

  def doWhenTransactionReceivedACK(transactionId: Int) = {

    println(this.self.path + ": ACK from " + sender().path + " received!")

    pendingQueue.get(transactionId) match {
      case Some(pendingTransaction) =>
        pendingTransaction.waitingFor.contains(sender()) match {
          case true =>
            pendingTransaction.waitingFor.remove(sender())
            if (pendingTransaction.waitingFor.isEmpty) {
              pendingTransaction.respondTo ! TransactionReceived(pendingTransaction.transaction.id)
            }
          case false =>
        }
      case None => println("No pending ACK to send with this transaction id(" + transactionId + ")")
    }
  }
}

object HowAlgorithm {
  val Active = "Active"
  val Passive = "Passive"
  val Core = "Core"
}

trait HowAlgorithm {
  protected val how: String
}

trait Active extends HowAlgorithm {
  override val how = HowAlgorithm.Active
}

trait Passive extends HowAlgorithm {
  override val how = HowAlgorithm.Passive
}

trait Core extends HowAlgorithm {
  override val how = HowAlgorithm.Core
}

/**
 * A class to manage pending transactions, it stores the Nodes the Node need to receive an Acknowledgement of TransactionReceived
 * @param transaction The pending transaction
 * @param respondTo Respond to this Node when all Nodes have received the transaction
 * @param waitingFor Waiting for this list of Nodes
 */
case class PendingTransaction(transaction: Transaction, respondTo: ActorRef, waitingFor: mutable.HashMap[ActorRef, String])

/**
 * A transaction with an associated timestamp
 * @param transaction the transaction
 * @param timeStamp the timestamp (vectorClock)
 */
case class TempTransaction(transaction: Transaction, timeStamp: VectorClock.clockType)

/**
 * A node of the distributed system
 * @param startLinkedToNodes a list containing the Nodes this Node will send transactions
 */
abstract class Node(val startLinkedToNodes: mutable.HashMap[ActorRef, String] = mutable.HashMap[ActorRef, String]())
  extends Actor
  with WhenAlgorithm
  with HowAlgorithm {

  protected var linkedToNodes = new mutable.HashMap[ActorRef, String]()
  linkedToNodes ++= startLinkedToNodes

  /**
   * The node vector clock
   */
  protected var vectorClock = VectorClock(VectorClock.Zero)

  def receive = {

    // If the node receives a message...
    case msg: Message =>

      vectorClock inc()

      msg match {

        case contentMsg: ContentMessage =>
          vectorClock updateWith contentMsg.vectorClock

          contentMsg.content match {
            case transaction: Transaction =>
              doWhenTransactionReceived(transaction, sender())
          }

        case controlMsg: ControlMessage =>

          controlMsg match {
            case TransactionReceived(transactionId) => doWhenTransactionReceivedACK(transactionId)
            case VectorClockUpdate(vc) => vectorClock updateWith vc
            case dMsg: DiscoveryAndLookupMessage =>
              dMsg match {
                case ReplicationType_?() => sender ! ReplicationType(how)
                case NewNode(node) =>
                  addNode(node)
              }
            case mngC: ManageConnection =>
              mngC match {
                case RequestNewConnection(id,factoryMethod) =>
                  val portRef = context.actorOf(Props(factoryMethod()),id.toInt.toString)
                  addNode(portRef)
                  portRef ! NewNode(self)
                  sender ! ResponseConnectionCreated(id,portRef)
                case _ =>
              }
          }
      }
  }

  def addNode(node: ActorRef): Unit = {
    implicit val ec = context.dispatcher
    implicit val timeout = Timeout(1.seconds)
    val future = node ? ReplicationType_?()

    future.onSuccess {
      case result =>
        linkedToNodes += node ->  result.asInstanceOf[ReplicationType].replicationType
        println(self.path + " added " + node.path + " as " + result.asInstanceOf[ReplicationType].replicationType)
    }
  }
}
