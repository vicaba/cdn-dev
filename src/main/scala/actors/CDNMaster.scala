package actors

import akka.actor.{ActorRef, Actor}
import net.message.{DiscoveryAndLookupMessage, ControlMessage, ContentMessage, Message}
import transaction.{ReadWriteTransaction, ReadOnlyTransaction, Transaction}
import transaction.operation.{ReadOperation, WriteOperation}

import scala.collection.mutable
import scala.collection.mutable.ListBuffer
import scala.util.Random

/**
 * Created by Work on 11/11/14.
 */

case class AddNode(node: ActorRef, idPart: Int, mode: Int) extends DiscoveryAndLookupMessage
case class PartitionUpdate(idPart: Int) extends DiscoveryAndLookupMessage

object AddNode {
  val WriteMode = 1
  val ReadMode = 0
}

class CDNMaster extends Actor {

  private val partitions = new mutable.HashMap[Int, Partition]()

  private val pendingConnections = new mutable.HashMap[Double, ListBuffer[ActorRef]]()

  private var manageConnectionId: Double = 0

  private def nextManageConnectionId(): Double = {
    manageConnectionId += 1
    manageConnectionId
  }

  private def addPartition(idPart: Int): Partition = {
    val p = new Partition
    partitions += idPart -> new Partition
    p
  }

  /**
   * The CDNMaster vector clock
   */
  private val vectorClock = VectorClock(VectorClock.Zero)

  def receive = {

    // If the actor receives a message...
    case msg: Message =>
      vectorClock inc()

      msg match {

        case contentMsg: ContentMessage =>
          vectorClock updateWith contentMsg.vectorClock

          contentMsg.content match {
            case transaction: Transaction =>
              transaction match {
                case ReadOnlyTransaction(_, _, _) => sendReadOnlyTransaction(transaction)
                case ReadWriteTransaction(_, _, _) => sendReadWriteTransaction(transaction)
              }
          }

        case controlMsg: ControlMessage =>
          controlMsg match {
            //case TransactionReceived(transactionId) => doWhenTransactionReceivedACK(transactionId)
            case VectorClockUpdate(vc) => vectorClock updateWith vc
            case PartitionUpdate(idPart) => setWriteOperation(idPart)
            case dMsg: DiscoveryAndLookupMessage =>
              dMsg match {
                case AddNode(node, idPart, mode) =>
                  // Get the partition if exists, if not, create one
                  val partition = partitions.getOrElse(idPart,
                  {
                    val p = new Partition
                    partitions += idPart -> p
                    p
                  })

                  val readList = partition.readList
                  readList.contains(node) match {
                    case false => readList += node
                    case true => println("Error: Duplicated node in readList")
                  }
                  val writeList = partition.writeList
                  writeList.contains(node) match {
                    case false =>
                      mode match {
                        case AddNode.WriteMode => writeList += node
                        case AddNode.ReadMode => println("Node on read mode")
                      }
                    case true => println("Error: Duplicated node in writeList")
                  }
              }
            case RequestNodeConnection(node1, node2) =>
              val id = nextManageConnectionId()

              pendingConnections += id -> new ListBuffer[ActorRef]

              val factory1 = PortNode.methodForInstantiation(node1._2, node1._3)
              val factory2 = PortNode.methodForInstantiation(node2._2, node2._3)

              node1._1 ! RequestNewConnection(id, factory1)
              node2._1 ! RequestNewConnection(id, factory2)

            case ResponseConnectionCreated(id, port) =>
              pendingConnections.get(id) match {
                case None => println("Something goes wrong with the link creation")
                case Some(list) =>
                  list.+=(port)
                  if (list.size == 2) {
                    val port1 = list.head
                    val port2 = list.last
                    port1 ! NewNode(port2)
                    port2 ! NewNode(port1)
                  }
              }
            case TransactionReceived(id) =>
              println("Transaction " + id + ", job done!")
          }
      }
  }

  private def sendReadOnlyTransaction(transaction: Transaction): Unit = {
    val idPart = transaction.idPart
    partitions.get(idPart) match {
      case None => println("Error: no such partition " + idPart)
      case Some(partition) =>
        val keys = partition.readList
        val node = keys.toVector(Random.nextInt(keys.size))
        // Send the message
        node ! ContentMessage(VectorClock.Zero, transaction)
    }
  }

  private def sendReadWriteTransaction(transaction: Transaction): Unit = {
    val idPart = transaction.idPart
    partitions.get(idPart) match {
      case None => println("Error: no such partition " + idPart)
      case Some(partition) =>
        partition.writeOperation match {
          case true =>
            val keys = partition.writeList
            val node = keys.toVector(Random.nextInt(keys.size))
            // Send the message
            node ! ContentMessage(VectorClock.Zero, transaction)
            println("Transaction " + transaction.id + " sent to " + node.path.name + " from partition " + idPart)
          case false => println("Error: partition " + idPart + " not writable")
        }
    }
  }

  def setWriteOperation(idPart: Int): Unit = {
    partitions.get(idPart) match {
      case None => println("Error: no such partition " + idPart)
      case Some(partition) => {
        partition.writeOperation = true
//        println("Partition " + idPart + " is now writable")
      }
    }
  }

  private class Partition {
    lazy val readList = new mutable.HashSet[ActorRef]()
    lazy val writeList = new mutable.HashSet[ActorRef]()
    var writeOperation = false
  }

  def main(args: Array[String]) {

  }

}