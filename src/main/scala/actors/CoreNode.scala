package actors

import akka.actor.ActorRef
import transaction.{Transaction, DB}

/**
 * Created by Work on 28/11/14.
 */
class CoreNode
  extends Node
  with Eager
  with Core
{

  protected val db = new DB()

  override def persistTransaction(transaction: Transaction): (Option[Transaction], Option[Transaction]) =
    (Some(transaction), Some(db.executeTransaction(transaction)))

}
