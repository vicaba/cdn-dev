package actors

import transaction.Transaction

/**
 * Created by Xavier on 29/11/2014.
 */
abstract class PortNode extends Node with WhenAlgorithm with HowAlgorithm {
  override def persistTransaction(transaction: Transaction): (Option[Transaction], Option[Transaction]) = (Some(transaction), None)
}

object PortNode {
  def methodForInstantiation(howAlgorithm: String, whenAlgorithm: String): () => PortNode = {
    howAlgorithm match {
      case HowAlgorithm.Active =>
        whenAlgorithm match {
          case WhenAlgorithm.Eager => () => new PortNode with Eager with Active
          case WhenAlgorithm.Lazy => () => new PortNode with Lazy with Active
          case _ => () => new PortNode with Eager with Active
        }
      case HowAlgorithm.Passive =>
        whenAlgorithm match {
          case WhenAlgorithm.Eager => () => new PortNode with Eager with Passive
          case WhenAlgorithm.Lazy => () => new PortNode with Lazy with Passive
          case _ => () => new PortNode with Eager with Passive
        }
      case _ => () => new PortNode with Eager with Active
    }
  }
}